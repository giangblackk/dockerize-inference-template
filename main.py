import shutil
import argparse
import pandas as pd

def parse_args():
    parser = argparse.ArgumentParser(description="Correcting texts in an input file")
    parser.add_argument("input_file", type=str, help="Path to the input text file")
    parser.add_argument("output_file", type=str, help="Path to the output corrected text file")
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    arguments = parse_args()
    input_file = arguments.input_file
    output_file = arguments.output_file
    shutil.copy(input_file, output_file)
